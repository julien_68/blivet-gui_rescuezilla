#!/bin/sh
echo -e '\033[42;01;1m Changement de la source du dépôt \033[m'
sudo sed -i 's/archive.ubuntu.com/ftp.uni-stuttgart.de/g' /etc/apt/sources.list
echo -e '\033[42;01;1m Mise à jour du dépôt \033[m'
sudo apt-get update
echo -e '\033[42;01;1m Installation des dépendances \033[m'
sudo apt-get install -y python3-blockdev python3-bytesize python3-pyudev python3-selinux python3-parted lvm2-dbusd adwaita-icon-theme git python3-pip python3-gi-cairo gir1.2-gtk-3.0 gir1.2-blockdev-2.0 libblockdev-lvm2 libblockdev-btrfs2 libblockdev-swap2 libblockdev-loop2 libblockdev-crypto2 libblockdev-mpath2 libblockdev-dm2 libblockdev-mdraid2 libblockdev-nvdimm2
sudo apt-get install -y python3-pip
echo -e '\033[42;01;1m Installation de blivet-gui via python3 \033[m'
sudo pip3 install pid
sudo pip3 install git+http://github.com/storaged-project/blivet.git@3.4-release
sudo pip3 install git+http://github.com/storaged-project/blivet-gui.git
echo -e '\033[42;01;1m Création du raccourci sur le bureau \033[m'
wget https://gitlab.com/julien_68/blivet-gui_rescuezilla/-/raw/main/blivet-gui_desktop -O- | tr -d '\r' > /home/ubuntu/.local/share/applications/blivet-gui.desktop
sleep 1
chmod 664 /home/ubuntu/.local/share/applications/blivet-gui.desktop
chown ubuntu:ubuntu /home/ubuntu/.local/share/applications/blivet-gui.desktop
ln -s /home/ubuntu/.local/share/applications/blivet-gui.desktop /home/ubuntu/Desktop/
echo -e '\033[42;01;1m Lancement de Blivet-gui \033[m'
sudo blivet-gui
sleep 1
clear
echo 'raccourci sur le bureau'
sleep 1
clear
sleep 1
echo 'raccourci sur le bureau'
sleep 1
clear
sleep 1
echo 'raccourci sur le bureau'
sleep 1
exit
